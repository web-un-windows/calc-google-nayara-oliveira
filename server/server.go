package server

import (
	"fmt"
	"log"
	"net/http"

	"main/calculadora"
)

const portNum string = ":8080"

func Server() {
	log.Println("Starling our simple http server.")

	http.HandleFunc("/", calculadora.Home)
	http.HandleFunc("/info", calculadora.Info)

	log.Println("Started on port", portNum)
	fmt.Println("To close connection CTRL+C :-)")

	err := http.ListenAndServe(portNum, nil)
	if err != nil {
		log.Fatal(err)
	}
}
